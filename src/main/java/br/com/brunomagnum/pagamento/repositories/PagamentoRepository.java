package br.com.brunomagnum.pagamento.repositories;

import br.com.brunomagnum.pagamento.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    Iterable<Pagamento> findBycartaoId(Integer id);
}
