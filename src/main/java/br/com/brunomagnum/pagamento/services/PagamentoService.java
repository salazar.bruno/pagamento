package br.com.brunomagnum.pagamento.services;

import br.com.brunomagnum.pagamento.clients.CartaoClient;
import br.com.brunomagnum.pagamento.models.Cartao;
import br.com.brunomagnum.pagamento.models.Pagamento;
import br.com.brunomagnum.pagamento.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;

    public Pagamento criaPagamento(Pagamento pagamento){
        Cartao cartao = cartaoClient.buscaCartaoPorId(pagamento.getCartaoId());

        if (cartao != null) {
            return pagamentoRepository.save(pagamento);
        } else {
            throw new RuntimeException("Cartao não ecziste");
        }
    }

    public Iterable<Pagamento> buscaPagamentosPorCartao(Integer cartao_id){

        try{
            return pagamentoRepository.findBycartaoId(cartao_id);
        } catch (RuntimeException exception){
            throw new RuntimeException("Nenhum pagamento com o cartão informado");
        }
    }

}
