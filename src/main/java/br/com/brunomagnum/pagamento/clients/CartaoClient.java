package br.com.brunomagnum.pagamento.clients;

import br.com.brunomagnum.pagamento.models.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "Cartao")
public interface CartaoClient {

    @GetMapping("/cartao/id/{cartao_id}")
    Cartao buscaCartaoPorId(@PathVariable Integer cartao_id);
}
