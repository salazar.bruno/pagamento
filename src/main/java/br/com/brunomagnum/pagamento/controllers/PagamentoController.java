package br.com.brunomagnum.pagamento.controllers;

import br.com.brunomagnum.pagamento.models.Pagamento;
import br.com.brunomagnum.pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    PagamentoService pagamentoService;

    @PostMapping
    public ResponseEntity<Pagamento> criaPagamento(@RequestBody Pagamento pagamento){
        try{
            return ResponseEntity.status(201).body(pagamentoService.criaPagamento(pagamento));

        } catch(RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,exception.getMessage());
        }
    }

    @GetMapping("/{cartao_id}")
    public Iterable<Pagamento> buscaPagamentosPorCartao(@PathVariable(name="cartao_id") Integer cartao_id){
        try{
            return pagamentoService.buscaPagamentosPorCartao(cartao_id);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,exception.getMessage());
        }
    }
}
